# NYC Schools

This project aims to provide useful information on NYC High schools for an actual student

## Why it's important to have this informative app for an actual student?

Choosing a high school is a significant decision for students and their families. There are many factors to consider, such as academic programs, extracurricular activities, location, and safety. An app that provides comprehensive information on NYC high schools can help students and their families make informed decisions about which school is the best fit for them.

## Demo

[Video preview](https://gitlab.com/general9811211/20230316-OscarMoreno-NYCSchools/-/raw/main/demo.webm)

## Screenshots

![Screenshot_1678976433](https://gitlab.com/general9811211/20230316-OscarMoreno-NYCSchools/-/raw/main/Screenshot_1678976433.png){width=30%} ![Screenshot_1678976438](https://gitlab.com/general9811211/20230316-OscarMoreno-NYCSchools/-/raw/main/Screenshot_1678976438.png){width=30%} ![Screenshot_1678976447](https://gitlab.com/general9811211/20230316-OscarMoreno-NYCSchools/-/raw/main/Screenshot_1678976447.png){width=30%} ![Screenshot_1678976451](https://gitlab.com/general9811211/20230316-OscarMoreno-NYCSchools/-/raw/main/Screenshot_1678976451.png){width=30%}

## Feature overview

*   [x] **Easy to read** like a directory
*   [x] **Schools details** for fast orientation
*   [x] **Visuals** to keep users engaged

```bash
Copyright 2023 Oscar Gustavo Moreno Bautista

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
